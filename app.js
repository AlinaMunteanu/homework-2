function addTokens(input, tokens){
  

    if(input.length <= 6){
        throw new Error('Input should have at least 6 characters');
    };
    
    if(typeof(input) !== "string"){
        throw new Error('Invalid input');
    }
    
    for(let i in tokens)
    {
        if(typeof(tokens[i].tokenName) !== "string")
        {
            throw new Error('Invalid array format');
        }
    }
     
    let inputReplaced = input;
    for( let i in tokens)
    {
        if(input.indexOf('...') !== -1)
        {
            inputReplaced = inputReplaced.replace('...','$' + '{' + tokens[i].tokenName + '}');
        }
    }
    return inputReplaced; 
}

    

           

    


const app = {
    addTokens: addTokens
}

module.exports = app;